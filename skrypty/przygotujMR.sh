#!/bin/bash

# Funkcja pobiera nazwę brancha na którym jesteśmy 
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}

czy_push() {
     read -p "Czy pushować tego brancha (t/n)? " wybor
       case "$wybor" in 
  	 t|T ) git push --set-upstream origin HEAD;;
  	 * ) ;;
       esac;
}


# Zmienne
branch=$(parse_git_branch)
poczatek=${branch:0:6}
reszta=${branch//[0-9]/}
task=${poczatek//[!0-9]/}
mdevel="$task-devel$reszta"
mtest="$task-test$reszta"

# Skrypt korzysta z "git add ." zatem przed użyciem trzeba się upewnić, że mamy przygotowane tylko odpowiednie zmiany.
# Nie może być innych cyfr niż numer taska w nazwie brancha.

# Przykładowe użycia (należy być na branchu, który chcemy uszykować)
# przygotujMR mh wpisuję komentarz do commita (dodaje, commituje zmiany z podanym komentarzem oraz merguje branche devel i test)
# przygotujMR mf (dodaje, commituje zmiany z komentarzem z nazwy brancha oraz merguje branch devel)

# Argumenty
# nic, albo mf lub mh (bierze nazwę brancha jako komentarz do commita)
# komentarz do commita (obojętne czy jest -mf albo -mh)
# mf (przechodzi na brancha, np. "12345-devel_komentarz" i merguje z "12345_komentarz")
# mh (przechodzi na brancha, np. "12345-test_komentarz" i merguje z "12345_komentarz", 
#      następnie przechodzi na brancha, np. "12345-devel_komentarz" i merguje z "12345-test_komentarz")

if [ $# = 0 ]; then
	git add .
	git commit -m "Dotyczy #${branch//'_'/ }"
	$(czy_push)
elif [[ $1 = 'mf' && $# == 1 ]]; then
	git add .
        git commit -m "Dotyczy #${branch//'_'/ }"
	$(czy_push)
        git checkout $mdevel
	git merge $branch
	$(czy_push)
        git checkout $branch
elif [[ $1 = 'mf' && $# > 1 ]]; then
        git add .
        git commit -m "Dotyczy #$task ${*:2}"
	$(czy_push)
        git checkout $mdevel
        git merge $branch
        $(czy_push)	
	git checkout $branch
elif [[ $1 = 'mh' && $# == 1 ]]; then
	git add .
        git commit -m "Dotyczy #${branch//'_'/ }"
	$(czy_push)
	git checkout $mtest
        git merge $branch
	$(czy_push)
        git checkout $mdevel
        git merge $mtest
	$(czy_push)
	git checkout $branch
elif [[ $1 = 'mh' && $# > 1 ]]; then
	git add .
        git commit -m "Dotyczy #$task ${*:2}"
	$(czy_push)
        git checkout $mtest
        git merge $branch
	$(czy_push)
        git checkout $mdevel
        git merge $mtest
	$(czy_push)
	git checkout $branch
else
	git add .
        git commit -m "Dotyczy #$task $*"
	$(czy_push)
fi

