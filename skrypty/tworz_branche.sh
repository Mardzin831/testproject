#!/bin/bash
# $1 - hotfix/fix/devel h f d
# $2 - numer taska bez #
# $3 - nazwa brancha bez master/test/devel
# przykład użycia:
# ./skrypt_gl.sh h 12345 poprawienie_skryptu_urlopow
#   w powyższym przypadku utworzone zostaną trzy branche, wydzielone
#   z mastera, testa i devela (bo został podany argument "h").

tworz_branche () {

  if [ "$1" == 'h' ]; then
    branch_m="$2""_""$3"
    git checkout master
    git pull
    git branch "$branch_m"

    branch_t="$2""-test_""$3"
    git checkout test
    git pull
    git branch "$branch_t"

    branch_d="$2""-devel_""$3"
    git checkout devel
    git pull
    git branch "$branch_d"
    
    git checkout "$branch_m"


  elif [ $1 == 'f' ]; then

    branch_t="$2""_""$3"
    git checkout test
    git pull
    git branch "$branch_t"

    branch_d="$2""-devel_""$3"
    git checkout devel
    git pull
    git branch "$branch_d"
    
    git checkout "$branch_t"


  elif [ $1 == 'd' ]; then

    branch_d="$2""_""$3"
    git checkout devel
    git pull
    git branch "$branch_d"
    
    git checkout "$branch_d"
  fi
}

tworz_branche $1 $2 $3
